import numpy as np
from dA import dA
import pickle
import matplotlib.pyplot as plt
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams


if __name__=="__main__":
    # load the dataset
    datafiles_names = ["6D_5_test_case_davide_moves",
                    "6D_5_test_case_davide2_moves",
                    "6D_5_test_case_davide3_moves"]

    dataset = []
    for name in datafiles_names:
        f = open("DavideDatasetJapan/"+name+".pickle","rb")
        dat = pickle.load(f)[0]
        dat /= np.linalg.norm(dat)
        dataset.append(dat)
        f.close()

    print "Data loaded ",[np.asarray(x).shape for x in dataset]
    for d in dataset:
        plt.figure()
        for g in d:
            plt.plot(g)
    # plt.show()

    # intialise the MODEL
    index = T.lscalar()    # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as rasterized images
    rng = np.random.RandomState(0xbeef)
    theano_rng = RandomStreams(rng.randint(2 ** 30))
    #time steps for the input signal window
    learning_rate=0.51
    training_epochs=350
    batch_size=20

    da = dA(
        numpy_rng=rng,
        theano_rng=theano_rng,
        input=x,
        n_visible=6,
        n_hidden=500
    )

    cost, updates = da.get_cost_updates(
        corruption_level=0.3,
        learning_rate=learning_rate
    )

    # make the train_set_x
    keep = dataset[1:]
    gestures = np.array(keep).flatten() #ten gestures

    gestures_flat = []
    for g in gestures:

        if gestures_flat == []:
            gestures_flat = g.tolist()
        else:
            gestures_flat.extend(g)
    # import pdb; pdb.set_trace()
    gestures_flat = theano.shared(np.asarray(gestures_flat,
                                    dtype=theano.config.floatX),
                                borrow=True)
    n_train_batches = gestures_flat.get_value(borrow=True).shape[0]\
                    // batch_size




    # train
    train_da = theano.function(
        [index],
        cost,
        updates=updates,
        givens={
            x: gestures_flat[index * batch_size: (index + 1) * batch_size]
        }
    )
    for epoch in range(training_epochs):
        # go through trainng set
        c = []
        for batch_index in range(n_train_batches):
            c.append(train_da(batch_index))

        print('Training epoch %d, cost ' % epoch, np.mean(c))

    # test
